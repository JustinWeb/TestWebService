﻿using MVWebAPITest.MoveIn;
using MVWebAPITest.MoveOut;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVWebAPITest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Encoding GetEncoding( int selectIndex )
        {
            if (selectIndex < 0)
            {
                return Encoding.ASCII;
            }
            else
            {
                switch (selectIndex)
                {
                    case 0:
                        return Encoding.UTF8;
                    case 1:
                        return Encoding.ASCII;
                    case 2:
                        return Encoding.Unicode;
                    default:
                        return Encoding.UTF8;
                }
            }
        }

        private string GetURL( int urlMode, bool isMoveIn )
        {
            switch (urlMode)
            {
                case 1: // DEV
                    return isMoveIn ? "http://tncmes07/COG/ChipMOS/WebService/OuterAFVMvinService.asmx" : "http://tncmes07/COG/ChipMOS/WebService/OuterAFVMvouService.asmx";
                   
                case 2: // PROD
                    return isMoveIn ? "http://cimescog.tn.chipmos.com.tw/COG/ChipMOS/WebService/OuterAFVMvinService.asmx" : "http://cimescog.tn.chipmos.com.tw/COG/ChipMOS/WebService/OuterAFVMvouService.asmx";
                    
                case 3: // URL
                    return isMoveIn ? this.txtMoveInURL.Text : this.txtMoveOutURL.Text;
                    
                default: // Use PROD
                    return isMoveIn ? "http://cimescog.tn.chipmos.com.tw/COG/ChipMOS/WebService/OuterAFVMvinService.asmx" : "http://cimescog.tn.chipmos.com.tw/COG/ChipMOS/WebService/OuterAFVMvouService.asmx";
                    
            }
        }

       
        static string LoadFromXml(string xmlFile)
        {
            StreamReader reader = new StreamReader(xmlFile, Encoding.UTF8);
            return reader.ReadToEnd();
        }

        static void SaveToFile(string fName, string data, Encoding saveEncoding)
        {
            StreamWriter writer = new StreamWriter(fName, false, saveEncoding);
            writer.Write(data);
            writer.Close();
        }

        private void btnOpenMoveOutXml_Click(object sender, EventArgs e)
        {
            DialogResult selectResult = this.openFileDialog1.ShowDialog();
            if (selectResult == System.Windows.Forms.DialogResult.OK)
            {
                this.txtMoveOutXml.Text = this.openFileDialog1.FileName;
            }
        }

        private void btnRunMoveOut_Click(object sender, EventArgs e)
        {
            if (this.txtMoveOutXml.Text.Length <= 0)
            {
                MessageBox.Show("Please select xml file");
                return;
            }

            try
            {
                int urlMode = 0;
                if (rdoEnvDEV.Checked)
                {
                    urlMode = 1;
                }
                else if (rdoEnvPROD.Checked)
                {
                    urlMode = 2;
                }
                else
                {
                    urlMode = 3;
                }
                if (rdoMoveIn.Checked)
                {
                    OuterRCAVMvinService moveIn = new OuterRCAVMvinService();
                    string xmlData = LoadFromXml(this.txtMoveOutXml.Text);
                    moveIn.Url = GetURL(urlMode, true);
                    string result = moveIn.DoOuterRCAVMvin(xmlData);
                    this.txtResult.Text = result;
                    SaveToFile("TestMoveIn.xml", result, GetEncoding(this.cmbEncodingMoveOut.SelectedIndex));
                }
                else
                {
                    OuterRCAVMvouService moveOut = new OuterRCAVMvouService();
                    string xmlData = LoadFromXml(this.txtMoveOutXml.Text);
                    moveOut.Url = GetURL(urlMode, false);
                    string result = moveOut.DoOuterRCAVMvou(xmlData);
                    this.txtResult.Text = result;
                    SaveToFile("TestMoveOut.xml", result, GetEncoding(this.cmbEncodingMoveOut.SelectedIndex));
                }
                
            }
            catch (Exception exp)
            {
                this.txtMessage.Text = exp.Message;
                //Console.WriteLine(exp.Message);
                //Console.WriteLine(exp.ToString());

                MessageBox.Show(exp.Message);
            }
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.cmbEncodingMoveOut.SelectedIndex = 0;
            this.txtMoveInURL.Text = new OuterRCAVMvinService().Url;
            this.txtMoveOutURL.Text = new OuterRCAVMvouService().Url;
        }
    }
}
