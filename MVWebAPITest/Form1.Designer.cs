﻿namespace MVWebAPITest
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabWebAPI = new System.Windows.Forms.TabControl();
            this.tpMoveOut = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdoMoveOut = new System.Windows.Forms.RadioButton();
            this.rdoMoveIn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoEnvPROD = new System.Windows.Forms.RadioButton();
            this.rdoEnvDEV = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbEncodingMoveOut = new System.Windows.Forms.ComboBox();
            this.txtMoveOutXml = new System.Windows.Forms.TextBox();
            this.btnOpenMoveOutXml = new System.Windows.Forms.Button();
            this.btnRunMoveOut = new System.Windows.Forms.Button();
            this.tabResult = new System.Windows.Forms.TabControl();
            this.tpResult = new System.Windows.Forms.TabPage();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.tpMessage = new System.Windows.Forms.TabPage();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtMoveInURL = new System.Windows.Forms.TextBox();
            this.txtMoveOutURL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rdoEnvURL = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabWebAPI.SuspendLayout();
            this.tpMoveOut.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabResult.SuspendLayout();
            this.tpResult.SuspendLayout();
            this.tpMessage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabWebAPI);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabResult);
            this.splitContainer1.Size = new System.Drawing.Size(968, 619);
            this.splitContainer1.SplitterDistance = 284;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabWebAPI
            // 
            this.tabWebAPI.Controls.Add(this.tpMoveOut);
            this.tabWebAPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabWebAPI.Location = new System.Drawing.Point(0, 0);
            this.tabWebAPI.Name = "tabWebAPI";
            this.tabWebAPI.SelectedIndex = 0;
            this.tabWebAPI.Size = new System.Drawing.Size(968, 284);
            this.tabWebAPI.TabIndex = 0;
            // 
            // tpMoveOut
            // 
            this.tpMoveOut.Controls.Add(this.panel2);
            this.tpMoveOut.Location = new System.Drawing.Point(4, 25);
            this.tpMoveOut.Name = "tpMoveOut";
            this.tpMoveOut.Padding = new System.Windows.Forms.Padding(3);
            this.tpMoveOut.Size = new System.Drawing.Size(960, 255);
            this.tpMoveOut.TabIndex = 1;
            this.tpMoveOut.Text = "Move In/Out";
            this.tpMoveOut.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cmbEncodingMoveOut);
            this.panel2.Controls.Add(this.txtMoveOutXml);
            this.panel2.Controls.Add(this.btnOpenMoveOutXml);
            this.panel2.Controls.Add(this.btnRunMoveOut);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(954, 249);
            this.panel2.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdoMoveOut);
            this.groupBox2.Controls.Add(this.rdoMoveIn);
            this.groupBox2.Location = new System.Drawing.Point(169, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(167, 111);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Move In/Out";
            // 
            // rdoMoveOut
            // 
            this.rdoMoveOut.AutoSize = true;
            this.rdoMoveOut.Location = new System.Drawing.Point(6, 58);
            this.rdoMoveOut.Name = "rdoMoveOut";
            this.rdoMoveOut.Size = new System.Drawing.Size(82, 19);
            this.rdoMoveOut.TabIndex = 11;
            this.rdoMoveOut.TabStop = true;
            this.rdoMoveOut.Text = "MoveOut";
            this.rdoMoveOut.UseVisualStyleBackColor = true;
            // 
            // rdoMoveIn
            // 
            this.rdoMoveIn.AutoSize = true;
            this.rdoMoveIn.Checked = true;
            this.rdoMoveIn.Location = new System.Drawing.Point(6, 33);
            this.rdoMoveIn.Name = "rdoMoveIn";
            this.rdoMoveIn.Size = new System.Drawing.Size(73, 19);
            this.rdoMoveIn.TabIndex = 10;
            this.rdoMoveIn.TabStop = true;
            this.rdoMoveIn.Text = "MoveIn";
            this.rdoMoveIn.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoEnvURL);
            this.groupBox1.Controls.Add(this.rdoEnvPROD);
            this.groupBox1.Controls.Add(this.rdoEnvDEV);
            this.groupBox1.Location = new System.Drawing.Point(15, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(133, 111);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Environment";
            // 
            // rdoEnvPROD
            // 
            this.rdoEnvPROD.AutoSize = true;
            this.rdoEnvPROD.Location = new System.Drawing.Point(19, 58);
            this.rdoEnvPROD.Name = "rdoEnvPROD";
            this.rdoEnvPROD.Size = new System.Drawing.Size(65, 19);
            this.rdoEnvPROD.TabIndex = 1;
            this.rdoEnvPROD.TabStop = true;
            this.rdoEnvPROD.Tag = "2";
            this.rdoEnvPROD.Text = "PROD";
            this.rdoEnvPROD.UseVisualStyleBackColor = true;
            // 
            // rdoEnvDEV
            // 
            this.rdoEnvDEV.AutoSize = true;
            this.rdoEnvDEV.Checked = true;
            this.rdoEnvDEV.Location = new System.Drawing.Point(19, 33);
            this.rdoEnvDEV.Name = "rdoEnvDEV";
            this.rdoEnvDEV.Size = new System.Drawing.Size(57, 19);
            this.rdoEnvDEV.TabIndex = 0;
            this.rdoEnvDEV.TabStop = true;
            this.rdoEnvDEV.Tag = "1";
            this.rdoEnvDEV.Text = "DEV";
            this.rdoEnvDEV.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Encoding";
            // 
            // cmbEncodingMoveOut
            // 
            this.cmbEncodingMoveOut.FormattingEnabled = true;
            this.cmbEncodingMoveOut.Items.AddRange(new object[] {
            "UTF-8",
            "Big5",
            "Unicode"});
            this.cmbEncodingMoveOut.Location = new System.Drawing.Point(129, 60);
            this.cmbEncodingMoveOut.Name = "cmbEncodingMoveOut";
            this.cmbEncodingMoveOut.Size = new System.Drawing.Size(135, 23);
            this.cmbEncodingMoveOut.TabIndex = 6;
            // 
            // txtMoveOutXml
            // 
            this.txtMoveOutXml.Location = new System.Drawing.Point(129, 19);
            this.txtMoveOutXml.Name = "txtMoveOutXml";
            this.txtMoveOutXml.Size = new System.Drawing.Size(797, 25);
            this.txtMoveOutXml.TabIndex = 5;
            // 
            // btnOpenMoveOutXml
            // 
            this.btnOpenMoveOutXml.Location = new System.Drawing.Point(15, 19);
            this.btnOpenMoveOutXml.Name = "btnOpenMoveOutXml";
            this.btnOpenMoveOutXml.Size = new System.Drawing.Size(95, 25);
            this.btnOpenMoveOutXml.TabIndex = 4;
            this.btnOpenMoveOutXml.Text = "Open";
            this.btnOpenMoveOutXml.UseVisualStyleBackColor = true;
            this.btnOpenMoveOutXml.Click += new System.EventHandler(this.btnOpenMoveOutXml_Click);
            // 
            // btnRunMoveOut
            // 
            this.btnRunMoveOut.Location = new System.Drawing.Point(15, 209);
            this.btnRunMoveOut.Name = "btnRunMoveOut";
            this.btnRunMoveOut.Size = new System.Drawing.Size(95, 31);
            this.btnRunMoveOut.TabIndex = 3;
            this.btnRunMoveOut.Text = "Run";
            this.btnRunMoveOut.UseVisualStyleBackColor = true;
            this.btnRunMoveOut.Click += new System.EventHandler(this.btnRunMoveOut_Click);
            // 
            // tabResult
            // 
            this.tabResult.Controls.Add(this.tpResult);
            this.tabResult.Controls.Add(this.tpMessage);
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.SelectedIndex = 0;
            this.tabResult.Size = new System.Drawing.Size(968, 331);
            this.tabResult.TabIndex = 0;
            // 
            // tpResult
            // 
            this.tpResult.Controls.Add(this.txtResult);
            this.tpResult.Location = new System.Drawing.Point(4, 25);
            this.tpResult.Name = "tpResult";
            this.tpResult.Padding = new System.Windows.Forms.Padding(3);
            this.tpResult.Size = new System.Drawing.Size(960, 302);
            this.tpResult.TabIndex = 0;
            this.tpResult.Text = "Result";
            this.tpResult.UseVisualStyleBackColor = true;
            // 
            // txtResult
            // 
            this.txtResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResult.Location = new System.Drawing.Point(3, 3);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(954, 296);
            this.txtResult.TabIndex = 0;
            // 
            // tpMessage
            // 
            this.tpMessage.Controls.Add(this.txtMessage);
            this.tpMessage.Location = new System.Drawing.Point(4, 25);
            this.tpMessage.Name = "tpMessage";
            this.tpMessage.Padding = new System.Windows.Forms.Padding(3);
            this.tpMessage.Size = new System.Drawing.Size(960, 302);
            this.tpMessage.TabIndex = 1;
            this.tpMessage.Text = "Message";
            this.tpMessage.UseVisualStyleBackColor = true;
            // 
            // txtMessage
            // 
            this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessage.Location = new System.Drawing.Point(3, 3);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(954, 296);
            this.txtMessage.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtMoveOutURL);
            this.groupBox3.Controls.Add(this.txtMoveInURL);
            this.groupBox3.Location = new System.Drawing.Point(373, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(569, 99);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "URL";
            // 
            // txtMoveInURL
            // 
            this.txtMoveInURL.Location = new System.Drawing.Point(52, 24);
            this.txtMoveInURL.Name = "txtMoveInURL";
            this.txtMoveInURL.Size = new System.Drawing.Size(501, 25);
            this.txtMoveInURL.TabIndex = 0;
            // 
            // txtMoveOutURL
            // 
            this.txtMoveOutURL.Location = new System.Drawing.Point(52, 58);
            this.txtMoveOutURL.Name = "txtMoveOutURL";
            this.txtMoveOutURL.Size = new System.Drawing.Size(501, 25);
            this.txtMoveOutURL.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "IN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "OUT";
            // 
            // rdoEnvURL
            // 
            this.rdoEnvURL.AutoSize = true;
            this.rdoEnvURL.Location = new System.Drawing.Point(19, 83);
            this.rdoEnvURL.Name = "rdoEnvURL";
            this.rdoEnvURL.Size = new System.Drawing.Size(56, 19);
            this.rdoEnvURL.TabIndex = 2;
            this.rdoEnvURL.TabStop = true;
            this.rdoEnvURL.Tag = "3";
            this.rdoEnvURL.Text = "URL";
            this.rdoEnvURL.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 619);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "MV WebAPI Test 20180805";
            this.Load += new System.EventHandler(this.OnLoad);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabWebAPI.ResumeLayout(false);
            this.tpMoveOut.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabResult.ResumeLayout(false);
            this.tpResult.ResumeLayout(false);
            this.tpResult.PerformLayout();
            this.tpMessage.ResumeLayout(false);
            this.tpMessage.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabWebAPI;
        private System.Windows.Forms.TabPage tpMoveOut;
        private System.Windows.Forms.TabControl tabResult;
        private System.Windows.Forms.TabPage tpResult;
        private System.Windows.Forms.TabPage tpMessage;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbEncodingMoveOut;
        private System.Windows.Forms.TextBox txtMoveOutXml;
        private System.Windows.Forms.Button btnOpenMoveOutXml;
        private System.Windows.Forms.Button btnRunMoveOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoEnvPROD;
        private System.Windows.Forms.RadioButton rdoEnvDEV;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdoMoveOut;
        private System.Windows.Forms.RadioButton rdoMoveIn;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtMoveOutURL;
        private System.Windows.Forms.TextBox txtMoveInURL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdoEnvURL;
    }
}

