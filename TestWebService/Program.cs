﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWebService
{
    class Program
    {
        /// <summary>
        /// 配合對方的WebService，利用已存在的xml資料檔來傳送及取得回報
        /// 執行時需給三個參數，
        /// 第一個是要執行的模式，可以是In 或　Out, 
        /// 第二個是要傳過去給對方的xml資料檔案路徑及名稱，這部份可以先手動編輯好一份來傳送，以便進行測試
        /// 第三個是收到的資料要用哪種編碼方式儲存，可以是UTF8, Big5, Unicode三種中的一種來儲存
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("TestWebService <mode> <xml> <encoding>");
                Console.WriteLine("<mode> : In | Out ");
                Console.WriteLine("<xml> : xml file for input");
                Console.WriteLine("<encoding> : UTF8 | Unicode | Big5");
            }
            else
            {
                Encoding encoding;
                switch (args[2])
                {
                    case "UTF8":
                        encoding = Encoding.UTF8;
                        break;
                    case "Big5":
                        encoding = Encoding.ASCII;
                        break;
                    case "Unicode":
                        encoding = Encoding.Unicode;
                        break;
                    default:
                        Console.WriteLine("Unknown encoding mode");
                        return;
                }

                switch (args[0])
                {
                    case "In":
                        TestMoveIn(args[1], encoding );
                        break;
                    case "Out":
                        TestMoveOut(args[1], encoding);
                        break;
                    default:
                        Console.WriteLine("Unknown mode");
                        break;
                }
            }
        }

        static void TestMoveIn( string xmlFile, Encoding saveEncoding )
        {
            try
            {
                MoveInService.OuterRCAVMvinService moveIn = new MoveInService.OuterRCAVMvinService();
                string xmlData = LoadFromXml(xmlFile);
                string result = moveIn.DoOuterRCAVMvin(xmlData);
                SaveToFile("TestMoveIn.xml", result, saveEncoding );
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                Console.WriteLine(exp.ToString());
            }
        }

        static void TestMoveOut(string xmlFile, Encoding saveEncoding)
        {
            try
            {
                MoveOutService.OuterRCAVMvouService moveOut = new MoveOutService.OuterRCAVMvouService();
                string xmlData = LoadFromXml(xmlFile);
                string result = moveOut.DoOuterRCAVMvou(xmlData);
                SaveToFile("TestMoveIn.xml", result, saveEncoding);
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                Console.WriteLine(exp.ToString());
            }
        }

        static string LoadFromXml(string xmlFile)
        {
            StreamReader reader = new StreamReader(xmlFile, Encoding.UTF8);
            return reader.ReadToEnd();            
        }

        static void SaveToFile(string fName, string data, Encoding saveEncoding)
        {
            StreamWriter writer = new StreamWriter(fName, false, saveEncoding);
            writer.Write(data);
            writer.Close();
        }
    }
}
